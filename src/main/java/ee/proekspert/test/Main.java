package ee.proekspert.test;

import static java.lang.System.err;
import static java.lang.System.out;

public class Main {

    public static void main(String[] args) throws Exception {
        long start = System.currentTimeMillis();

        if (args.length == 0 || args[0].equals("-h")) {
            printHelp();
        } else {
            runApplication(args);
        }

        out.printf("\nExecution time: %dms\n", System.currentTimeMillis() - start);
    }

    private static void runApplication(String[] args) {
        try {
            new Application(args).run();
        } catch (Exception e) {
            err.println(e.getMessage());
        }
    }

    public static void printHelp() {
        System.out.println();
        System.out.println("Syntax: java -jar <application.jar> <filename> <count>");
        System.out.println("Where: ");
        System.out.println("    <application.jar> - name of this jar file");
        System.out.println("    <filename>        - file name of the log file");
        System.out.println("    <count>           - number of resources with highest average request duration");
        System.out.println();
    }
}
