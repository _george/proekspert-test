package ee.proekspert.test.validator;

import java.nio.file.Files;
import java.nio.file.Paths;

public class Validator {
    public static void validateFileName(String fileName) {
        if (fileName == null || fileName.trim().isEmpty()) {
            throw new IllegalArgumentException("Enter file name please");
        }

        if (!Files.exists(Paths.get(fileName))) {
            throw new IllegalArgumentException("File '" + fileName + "' not exist");
        }
    }

    public static void validateCount(int count) {
        if (count < 1) {
            throw new IllegalArgumentException("<count> should be positive integer");
        }
    }
}
