package ee.proekspert.test;

import ee.proekspert.test.model.LogEntry;
import ee.proekspert.test.statistics.LogStatistics;
import ee.proekspert.test.validator.Validator;

import java.io.OutputStream;
import java.util.List;

public class Application {

    private String fileName;

    private int count;

    public Application(String[] args) {
        parseArguments(args);
        validateArguments();
    }

    private void parseArguments(String[] args) {
        if (args.length > 0) {
            this.fileName = args[0];
        }

        if (args.length > 1) {
            try {
                count = Integer.parseInt(args[1]);
            } catch (NumberFormatException e) {
            }
        }
    }

    public void run() throws Exception {
        printStatistics(LogParser.parse(fileName), System.out);
    }

    private void printStatistics(List<LogEntry> entries, OutputStream stream) {
        LogStatistics statistics = new LogStatistics(entries);
        statistics.printHourly(stream);
        statistics.printSlowResources(stream, count);
    }

    private void validateArguments() {
        Validator.validateFileName(fileName);
        Validator.validateCount(count);
    }

}
