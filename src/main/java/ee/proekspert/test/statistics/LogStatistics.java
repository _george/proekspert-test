package ee.proekspert.test.statistics;

import ee.proekspert.test.model.LogEntry;

import java.io.OutputStream;
import java.util.List;

public class LogStatistics {

    private HourlyStatistics hourlyStatistics;

    private AverageTimeStatistics averageTimeStatistics;

    public LogStatistics(List<LogEntry> entries) {
        hourlyStatistics = new HourlyStatistics(entries);
        averageTimeStatistics = new AverageTimeStatistics(entries);
    }

    public void printHourly(OutputStream stream) {
        hourlyStatistics.print(stream);
    }

    public void printSlowResources(OutputStream stream, int count) {
        averageTimeStatistics.print(stream, count);
    }
}
