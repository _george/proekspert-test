package ee.proekspert.test.statistics;

import ee.proekspert.test.model.LogEntry;

import java.io.IOException;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

public class HourlyStatistics {

    private Map<Integer, Integer> statistics;

    private int hourlyStatisticsWidth = 50;

    public HourlyStatistics(List<LogEntry> data) {
        statistics = generate(data);
    }

    private Map<Integer, Integer> generate(List<LogEntry> entries) {
        Map<Integer, Integer> statistics = new TreeMap<>();
        entries.forEach(logEntry -> addHourlyRecord(statistics, logEntry));
        return statistics;
    }

    private void addHourlyRecord(Map<Integer, Integer> statistics, LogEntry entry) {
        int hour = entry.getDateTime().getHour();
        Integer hits = statistics.get(hour);
        statistics.put(hour, hits == null ? 1 : ++hits);
    }

    public void print(OutputStream out) {
        StringBuilder sb = new StringBuilder("\n\n");
        sb.append("Hourly statistics\n");
        sb.append("==================================================\n");
        sb.append(" hour   hits\n");

        Integer maxHits = statistics.values().stream().max(Integer::compare).get();
        float divisor = (float) maxHits / hourlyStatisticsWidth;

        statistics.entrySet().forEach(entry -> appendHourStatistics(entry, divisor, sb));

        sb.append("==================================================\n\n");

        write(sb.toString(), out);
    }

    private void write(String str, OutputStream stream) {
        try {
            stream.write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void appendHourStatistics(Map.Entry<Integer, Integer> entry, float divisor, StringBuilder sb) {
        Integer hour = entry.getKey();
        Integer hits = entry.getValue();

        sb.append(String.format("[%4d][%5d] ", hour, hits));
        sb.append(makeHourPile(hits, divisor)).append("\n");
    }

    private char[] makeHourPile(Integer hits, float divisor) {
        int chars = Math.round((float) hits / divisor);
        chars = chars < 0 ? 0 : chars;

        char[] pile = new char[chars];

        Arrays.fill(pile, '%');

        return pile;
    }
}
