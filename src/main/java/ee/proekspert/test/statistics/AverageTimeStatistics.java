package ee.proekspert.test.statistics;

import ee.proekspert.test.model.LogEntry;
import ee.proekspert.test.validator.Validator;

import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AverageTimeStatistics {

    private Map<String, Float> statistics;

    public AverageTimeStatistics(List<LogEntry> entries) {
        statistics = generateStatistics(entries);
    }

    private Map<String, Float> generateStatistics(List<LogEntry> entries) {
        Map<String, Float> statistics = new TreeMap<>();

        entries.forEach(logEntry -> addAverageTimeRecord(statistics, logEntry));

        statistics.entrySet().forEach(this::round);

        return sortByValue(statistics, Comparator.reverseOrder());
    }

    private void addAverageTimeRecord(Map<String, Float> statistics, LogEntry logEntry) {
        Float averageTime = statistics.get(logEntry.getResource());

        if (averageTime == null) {
            averageTime = Float.valueOf(logEntry.getDuration());
        } else {
            averageTime = (averageTime + (float) logEntry.getDuration()) / 2;
        }

        statistics.put(logEntry.getResource(), averageTime);
    }

    private void round(Map.Entry<String, Float> mapEntry) {
        mapEntry.setValue((float) Math.round(mapEntry.getValue()));
    }

    private Map<String, Float> sortByValue(Map<String, Float> statistics, Comparator<Float> comparator) {
        Stream<Map.Entry<String, Float>> stream = statistics.entrySet().stream();
        stream = stream.sorted(Map.Entry.comparingByValue(comparator));
        return stream.collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
    }

    public void print(OutputStream out, int count) {
        Validator.validateCount(count);

        StringBuilder sb = new StringBuilder("\n\n");
        sb.append("Slow requests statistics\n");
        sb.append("==================================================\n");
        sb.append("  N    time\n");

        int i = 0;
        for (Map.Entry entry : statistics.entrySet()) {
            sb.append(String.format("[%3d][%5.0fms] %s\n", ++i, entry.getValue(), entry.getKey()));
            if (i >= count) {
                break;
            }
        }

        sb.append("==================================================\n\n");

        write(sb.toString(), out);
    }

    private void write(String str, OutputStream stream) {
        try {
            stream.write(str.getBytes());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
