package ee.proekspert.test;

import ee.proekspert.test.model.LogEntry;
import ee.proekspert.test.validator.Validator;

import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import static java.nio.file.Paths.get;

public class LogParser {

    private static final DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss,SSS");

    private LogParser() {
    }

    public static List<LogEntry> parse(String fileName) throws Exception {
        Validator.validateFileName(fileName);
        return Files.lines(get(fileName)).parallel().map(line -> convert(line)).collect(Collectors.toList());
    }

    private static LogEntry convert(String line) {
        LogEntry entry = new LogEntry();
        entry.setDateTime(extractDateTime(line));
        entry.setDuration(extractDuration(line));
        entry.setResource(extractResource(line));
        return entry;
    }

    private static String extractResource(String line) {
        int startIdx = line.indexOf(']') + 2; // + '] '
        int endIdx = line.indexOf(' ', startIdx);
        String queryString = line.substring(startIdx, endIdx);
        return removeMSISDN(queryString);
    }

    private static String removeMSISDN(String queryString) {
        return queryString.replaceAll("&?msisdn=[0-9]{12}", "");
    }

    private static int extractDuration(String line) {
        return Integer.valueOf(line.substring(line.lastIndexOf(' ') + 1));
    }

    private static LocalDateTime extractDateTime(String line) {
        return LocalDateTime.parse(line.substring(0, 23), DATE_TIME_FORMATTER);
    }


}
