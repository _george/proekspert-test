package ee.proekspert.test;

import ee.proekspert.test.model.LogEntry;
import ee.proekspert.test.statistics.LogStatistics;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class LogStatisticsTest {

    static List<LogEntry> entries;

    static {
        try {
            entries = LogParser.parse("src/test/resources/timing.log");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void printSlowResource() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        new LogStatistics(entries).printSlowResources(out, 10);

        String expected = new String(Files.readAllBytes(Paths.get("src/test/resources/statistics_slow_requests.txt")));
        String actual = out.toString();

        assertEquals(expected, actual);
    }

    @Test
    public void printHourly() throws Exception {
        ByteArrayOutputStream out = new ByteArrayOutputStream();

        new LogStatistics(entries).printHourly(out);

        String expected = new String(Files.readAllBytes(Paths.get("src/test/resources/statistics_hourly.txt")));
        String actual = out.toString();

        assertEquals(expected, actual);
    }

}