package ee.proekspert.test;

import ee.proekspert.test.model.LogEntry;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;

public class ParserTest {

    static String TEST_DATA_FILE = "src/test/resources/timing.log";

    @Test(expected = IllegalArgumentException.class)
    public void parseFileThatIsNull() throws Exception {
        LogParser.parse(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void parseNonExistingFile() throws Exception {
        LogParser.parse("file_not_exist");
    }

    @Test
    public void parse() throws Exception {
        List<LogEntry> list = LogParser.parse(TEST_DATA_FILE);
        assertEquals(1000, list.size());
    }

}