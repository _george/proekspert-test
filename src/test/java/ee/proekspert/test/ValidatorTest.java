package ee.proekspert.test;

import ee.proekspert.test.validator.Validator;
import org.junit.Test;

public class ValidatorTest {

    @Test(expected = IllegalArgumentException.class)
    public void validateFileIsNull() throws Exception {
        Validator.validateFileName(null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateFileEmpty() throws Exception {
        Validator.validateFileName("");
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateFileNotFound() {
        Validator.validateFileName("not_found_file");
    }
}